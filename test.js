import { Selector, t } from 'testcafe'

fixture `simple test`
  .page `https://duckduckgo.com/`

test('it works', async t =>  {
  await t.expect(Selector('.tag-home__item').innerText).contains('search engine')
})